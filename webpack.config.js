const path = require('path');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const htmlWebpackPlugin = new HtmlWebpackPlugin({
    template: path.join(__dirname, "./src/index.html"),
    filename: "./index.html"
});
module.exports = {
    entry: path.join(__dirname, "./src/index.js"),
    output: {
      path: path.resolve(__dirname, "build"),
      filename: "bundle.js",
      publicPath: '/'
    },
    optimization: {
      namedModules: false,
      namedChunks: true,
      minimize: true
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
              test: /\.(css|scss|sass)/,
              use: ['style-loader',
                {
                  loader: 'css-loader',
                  options: {
                    modules: false,
                    importLoaders: 1,
                    localIdentName: '[local]-[hash:base64:5]',
                  }
                },
                {
                  loader: "sass-loader",
                  options: {
                    sourceMap: true,
                  }
                },
                {
                  loader: `postcss-loader`,
                  options: {
                    options: {},
                  }
                },
              ]
            },
            {
              test: /\.(jpe?g|png|gif|svg|eot|svg|ttf|woff|woff2)$/i,
              use: ['file-loader'],
            }
        ]
    },
    devtool: 'inline-source-map',
    plugins: [
      new ExtractTextPlugin({filename: 'style.css'}),
      htmlWebpackPlugin
    ],
    resolve: {
        extensions: [".js", ".jsx"],
        alias: {
          components: path.resolve(__dirname, 'src/components/'),
          styles: path.resolve(__dirname, 'src/styles/'),
          fonts: path.resolve(__dirname, 'src/fonts/'),
          images: path.resolve(__dirname, 'src/images/'),
          redusers: path.resolve(__dirname, 'src/redusers/'),
          actions: path.resolve(__dirname, 'src/actions/'),
          constants: path.resolve(__dirname, 'src/constants/'),

        }
    },
    devServer: {
        port: 3001,
        hot: true,
        watchOptions: {
            poll: true
        }
    }
};
