import {
    ADD_ITEM,
    GET_ITEMS,
    DELETE_ITEM,
    EDIT_ITEM
} from 'constants';

const initialState = {
  items: [],
};

export default (app = initialState, action) => {
  const { type, data } = action;

    switch (type) {
        case ADD_ITEM:
        return {
            items: data
        };

        case GET_ITEMS:
            return {
                items: data
            };

        case DELETE_ITEM:
            return {
                items: data
            };

        case EDIT_ITEM:
            return {
                items: data
            };
            
      default:
        return app
    }
}
