import React, { Component } from 'react';
import Item from 'components/Item';
import './styles.scss';


class ListItems extends Component {
  render() {
      const { items } = this.props;
    return (
        <div className="list">
            {items.map((el, key) => (
                <Item
                    key={key}
                    {...el}
                    deleteItem={this.props.deleteItem}
                    editItem={this.props.editItem}
                />
            ))}
        </div>
    )
  }
}

export default ListItems;
