import React, { Component } from 'react';
import AddForm from 'components/AddForm';
import ListItems from 'components/ListItems';
import { connect } from 'react-redux';
import {
    getItems,
    deleteItem,
    editItem,
    addItem,
} from 'actions/app';
import './styles.scss';


class MainPage extends Component {
    componentDidMount() {
        this.props.getItems();
    }
  render() {
      const {
          getItems,
          addItem,
          deleteItem,
          editItem,
          items,
      } = this.props;

    return (
        <div className="wrap">
            <div className="form">
                <AddForm
                    addItem={addItem}
                />
                <ListItems
                    deleteItem={deleteItem}
                    addItem={addItem}
                    editItem={editItem}
                    items={items}
                />
            </div>
        </div>
    )
  }
}


const mapStateToProps = state => ({
    items: state.app.items
})

const mapDispatchToProps = {
  getItems,
  deleteItem,
  addItem,
  editItem,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
