import React, { Component } from 'react';
import './style.scss';


class Item extends Component {
    state = {
        value: '',
        isEdit: false,
    }

    toggleEdit = param => {
        this.setState({
            isEdit: param,
            value: this.props.value
        })
    }

    handleChange = e => {
        this.setState({
            value: e.target.value
        })
    }

    handleStatus = () => {
        this.props.editItem({
            id: this.props.id,
            status: !this.props.status,
            value: this.state.value
        })
    }

    handleEdit = () => {
        this.props.editItem({
            id: this.props.id,
            status: this.props.status,
            value: this.state.value
        })

        this.setState({
            isEdit: false,
        })
    }

    handleDelete = () => {
        this.props.deleteItem(this.props.id);
    }
  render() {
      const { value, isEdit } = this.state;
    return (
        <div className="item">
            {
                !isEdit && this.props.status
                    ? <div className="item__line"></div>
                    : null
            }
            {
                isEdit
                    ? <input type="text" defaultValue={value} onChange={this.handleChange} />
                    : <div className="item__title">{this.props.value}</div>
            }
            {
                isEdit
                    ?  <div className="btn-group">
                         <button className="btn green" onClick={this.handleEdit}>[п]</button>
                         <button className="btn" onClick={() => this.toggleEdit(false)}>[o]</button>
                        </div>
                    : <div className="btn-group">
                        <button className="btn" onClick={this.handleDelete}>[y]</button>
                        <button className="btn" onClick={() => this.toggleEdit(true)}>[и]</button>
                        <button className="btn" onClick={this.handleStatus}>[з]</button>
                      </div>
            }
        </div>
    )
  }
}


export default Item;
