import React, { Component } from 'react';
import './styles.scss';

class AddForm extends Component {
    state = {
        value: ''
    }

    handleChange = e => {
        this.setState({
            value: e.target.value
        });
    }

    addItem = e => {
        const { value } = this.state;
        e.preventDefault();
        this.props.addItem({
            value,
            status: false
        });
        this.setState({
            value: ''
        })
    }
    render() {
        const { value } = this.state;
        return (
            <form onSubmit={this.addItem} className="add-form">
                <div className="add-form__input">
                    <input
                        type="text"
                        value={value}
                        onChange={this.handleChange}
                        placeholder="событие"
                    />
                </div>
                <div className="add-form__btn">
                    <input type="submit" value="Добавить"/>
                </div>
            </form>
        )
    }
}

export default AddForm;
