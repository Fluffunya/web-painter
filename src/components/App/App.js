import React, { Component } from 'react';
import MainPage from 'components/MainPage/MainPage';
import './styles.scss';

class App extends Component {
  render() {
    return (
      <div className="Wrap">
        <MainPage />
    </div>
    )
  }
}

export default App;
