import {
    ADD_ITEM,
    GET_ITEMS,
    DELETE_ITEM,
    EDIT_ITEM
} from 'constants';

export const addItem = data => {
    let items = JSON.parse(localStorage.getItem('items')) || [];
    data.id = data.value + items.length;
    items.push(data);
    localStorage.setItem('items', JSON.stringify(items));
    return {
        type: ADD_ITEM,
        data: items
    }
}

export const getItems = () => {
    let data = JSON.parse(localStorage.getItem('items')) || [];

    return {
        type: GET_ITEMS,
        data: data
    }
}

export const deleteItem = id => {
    let items = JSON.parse(localStorage.getItem('items')) || [];
    let result = [];

    items.forEach(el => {
        if (el.id !== id ) {
            result.push(el)
        }
    });

    localStorage.setItem('items', JSON.stringify(result));

    return {
        type: DELETE_ITEM,
        data: result
    }
}

export const editItem = data => {
    let items = JSON.parse(localStorage.getItem('items')) || [];
    let result = [];

    items.forEach(el => {
        if (el.id === data.id ) {
            result.push(data);
        } else {
            result.push(el)
        }

    });

    localStorage.setItem('items', JSON.stringify(result));

    return {
        type: EDIT_ITEM,
        data: result,
    }
}
